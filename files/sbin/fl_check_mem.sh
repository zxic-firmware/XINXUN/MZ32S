#!/bin/sh

get_print_path()
{
	echo "for example：/dev/console or /tmp/mem.log"	
	echo "input you want print path:"	
	read  PRINT_PATH
}

show_process_meminfo()
{
    total_mem=0
	echo -e "     PID\t\tMem(Kb)\t\tProc_Name"
	for pid in `ls -l /proc | grep ^d | awk '{ print $9 }'| grep -v [^0-9]`
	do
		if [ $pid -eq 1 ];then
			continue;
		fi

		grep -q "VmRSS" /proc/$pid/status 2>/dev/null

		if [ $? -eq 0 ];then
			proc_name=$(grep Name /proc/$pid/status | awk '{ print $2 }')
			mem_size=$(grep VmRSS /proc/$pid/status | awk '{ print $2 }')
			i=1024
		    printf "%8s\t%10s Kb\t%16s\n" $pid $mem_size $proc_name	
		fi 
	done
} 

show_total_meminfo()
{
	echo "##########################################################"
	cat /proc/meminfo | sed -n '1p2p3p'
	echo "##########################################################"
}




PRINT_PATH=/dev/console 

echo "" > $PRINT_PATH   
{ 
	while true
	do

	echo "**********************************************************************"
	echo -e `date +"%y-%m-%d %H:%M:%S"`  

	show_total_meminfo;

	show_process_meminfo;

	echo "**********************************************************************"	
	sleep 3

	done 
}
