#!/bin/sh

main()
{
    local set_flag=`nv get fl_set_iptables_flag`
    local lan_ipaddr=`nv get lan_ipaddr`

    if [ "${set_flag}" == "1" ];then
        iptables -D INPUT -i br0 -d $lan_ipaddr -p tcp --dport 80 -j DROP
        iptables -D INPUT -i br0 -d $lan_ipaddr -p tcp --dport 443 -j DROP
        iptables -D FORWARD -i br0 -o wan1 -j DROP

        iptables -I INPUT -i br0 -d $lan_ipaddr -p tcp --dport 80 -j DROP
        iptables -I INPUT -i br0 -d $lan_ipaddr -p tcp --dport 443 -j DROP
        iptables -I FORWARD -i br0 -o wan1 -j DROP

        led_ctrl_cmd down_flow

        ifconfig usblan0 down
        sleep 1
        ifconfig eth0 down
        sleep 1
        ifconfig br0 down
        sleep 1

    fi

    if [ "${set_flag}" == "0" ];then
        iptables -D INPUT -i br0 -d $lan_ipaddr -p tcp --dport 80 -j DROP
        iptables -D INPUT -i br0 -d $lan_ipaddr -p tcp --dport 443 -j DROP
        iptables -D FORWARD -i br0 -o wan1 -j DROP

        ifconfig br0 up
        sleep 1
        ifconfig usblan0 up
        sleep 1
        ifconfig eth0 up
        sleep 1

        led_ctrl_cmd up_flow
    fi
}

main
