#!/bin/sh

LIGHT_WIFI="wifi_led"
LIGHT_SYS="syspwr_led"
LIGHT_NET=$LIGHT_SYS
LIGHT_MODEM_RED="4g_r_led"
LIGHT_MODEM_BLUE="4g_b_led"
LIGHT_BATT1="batt1_led"
LIGHT_BATT2="batt2_led"
LIGHT_BATT3="batt3_led"
LIGHT_BATT4="batt4_led"

CHARGER_ONLINE=/sys/class/power_supply/charger/online
BATTEERY_VOLTAGE=/sys/class/power_supply/battery/voltage_now
LEDS_CLASS=/sys/devices/platform/leds-gpio.1/leds

ON=1
OFF=0
BLINK=timer
SLOW=1000

BATT_VOL_FULL=4280
BATT_VOL_CUT=3350
BATT_VOL_VALID_DF=$((BATT_VOL_FULL - BATT_VOL_CUT))
BATT_VOL_DF1=$(expr $BATT_VOL_VALID_DF \* 5 / 100)
BATT_VOL_DF2=$(expr $BATT_VOL_VALID_DF \* 25 / 100)
BATT_VOL_DF3=$(expr $BATT_VOL_VALID_DF \* 50 / 100)
BATT_VOL_DF4=$(expr $BATT_VOL_VALID_DF \* 75 / 100)
BATT_VOL_DF5=$(expr $BATT_VOL_VALID_DF \* 100 / 100)
BATT_VOL_T1=$((BATT_VOL_CUT + BATT_VOL_DF1))
BATT_VOL_T2=$((BATT_VOL_CUT + BATT_VOL_DF2))
BATT_VOL_T3=$((BATT_VOL_CUT + BATT_VOL_DF3))
BATT_VOL_T4=$((BATT_VOL_CUT + BATT_VOL_DF4))
BATT_VOL_T5=$((BATT_VOL_CUT + BATT_VOL_DF5))





batt_ctrl()
{
    charger=$(cat $CHARGER_ONLINE)
    n_vlotage=$(sed 's/mV//g' $BATTEERY_VOLTAGE)
    [ -z $n_vlotage ] && n_vlotage=0
    if [ "${charger}" == "0" ]; then
        if [ $n_vlotage -gt $BATT_VOL_T4 ]; then
            echo $ON > $LEDS_CLASS/$LIGHT_BATT1/brightness
            echo $ON > $LEDS_CLASS/$LIGHT_BATT2/brightness
            echo $ON  > $LEDS_CLASS/$LIGHT_BATT3/brightness
            echo $ON  > $LEDS_CLASS/$LIGHT_BATT4/brightness
        elif [ $n_vlotage -gt $BATT_VOL_T3 ]; then
            echo $ON > $LEDS_CLASS/$LIGHT_BATT1/brightness
            echo $ON > $LEDS_CLASS/$LIGHT_BATT2/brightness
            echo $ON  > $LEDS_CLASS/$LIGHT_BATT3/brightness
            echo $OFF  > $LEDS_CLASS/$LIGHT_BATT4/brightness
        elif [ $n_vlotage -gt $BATT_VOL_T2 ]; then
            echo $ON > $LEDS_CLASS/$LIGHT_BATT1/brightness
            echo $ON > $LEDS_CLASS/$LIGHT_BATT2/brightness
            echo $OFF  > $LEDS_CLASS/$LIGHT_BATT3/brightness
            echo $OFF  > $LEDS_CLASS/$LIGHT_BATT4/brightness
        elif [ $n_vlotage -gt $BATT_VOL_T1 ]; then
            echo $ON > $LEDS_CLASS/$LIGHT_BATT1/brightness
            echo $OFF > $LEDS_CLASS/$LIGHT_BATT2/brightness
            echo $OFF  > $LEDS_CLASS/$LIGHT_BATT3/brightness
            echo $OFF  > $LEDS_CLASS/$LIGHT_BATT4/brightness
        else
            echo $BLINK  > $LEDS_CLASS/$LIGHT_BATT1/trigger
            echo $SLOW > $LEDS_CLASS/$LIGHT_BATT1/delay_on
            echo $SLOW > $LEDS_CLASS/$LIGHT_BATT1/delay_off
            echo $OFF > $LEDS_CLASS/$LIGHT_BATT2/brightness
            echo $OFF  > $LEDS_CLASS/$LIGHT_BATT3/brightness
            echo $OFF  > $LEDS_CLASS/$LIGHT_BATT4/brightness
        fi
    else # 充电状态 --- 灯闪烁
        if [ $n_vlotage -gt $BATT_VOL_T5 ]; then
            echo $ON > $LEDS_CLASS/$LIGHT_BATT1/brightness
            echo $ON > $LEDS_CLASS/$LIGHT_BATT2/brightness
            echo $ON  > $LEDS_CLASS/$LIGHT_BATT3/brightness
            echo $ON  > $LEDS_CLASS/$LIGHT_BATT4/brightness
        elif [ $n_vlotage -gt $BATT_VOL_T4 ]; then
            echo $ON > $LEDS_CLASS/$LIGHT_BATT1/brightness
            echo $ON  > $LEDS_CLASS/$LIGHT_BATT2/brightness
            echo $ON  > $LEDS_CLASS/$LIGHT_BATT3/brightness
            echo $BLINK  > $LEDS_CLASS/$LIGHT_BATT4/trigger
            echo $SLOW > $LEDS_CLASS/$LIGHT_BATT4/delay_on
            echo $SLOW > $LEDS_CLASS/$LIGHT_BATT4/delay_off
        elif [ $n_vlotage -gt $BATT_VOL_T3 ]; then
            echo $ON > $LEDS_CLASS/$LIGHT_BATT1/brightness
            echo $ON  > $LEDS_CLASS/$LIGHT_BATT2/brightness
            echo $BLINK  > $LEDS_CLASS/$LIGHT_BATT3/trigger
            echo $SLOW > $LEDS_CLASS/$LIGHT_BATT3/delay_on
            echo $SLOW > $LEDS_CLASS/$LIGHT_BATT3/delay_off
            echo $OFF  > $LEDS_CLASS/$LIGHT_BATT4/brightness
        elif [ $n_vlotage -gt $BATT_VOL_T2 ]; then
            echo $ON > $LEDS_CLASS/$LIGHT_BATT1/brightness
            echo $BLINK  > $LEDS_CLASS/$LIGHT_BATT2/trigger
            echo $SLOW > $LEDS_CLASS/$LIGHT_BATT2/delay_on
            echo $SLOW > $LEDS_CLASS/$LIGHT_BATT2/delay_off
            echo $OFF  > $LEDS_CLASS/$LIGHT_BATT3/brightness
            echo $OFF  > $LEDS_CLASS/$LIGHT_BATT4/brightness
        elif [ $n_vlotage -gt $BATT_VOL_T1 ]; then
            echo $BLINK  > $LEDS_CLASS/$LIGHT_BATT1/trigger
            echo $SLOW > $LEDS_CLASS/$LIGHT_BATT1/delay_on
            echo $SLOW > $LEDS_CLASS/$LIGHT_BATT1/delay_off
            echo $OFF > $LEDS_CLASS/$LIGHT_BATT2/brightness
            echo $OFF  > $LEDS_CLASS/$LIGHT_BATT3/brightness
            echo $OFF  > $LEDS_CLASS/$LIGHT_BATT4/brightness
        else
            echo $BLINK  > $LEDS_CLASS/$LIGHT_BATT1/trigger
            echo $SLOW > $LEDS_CLASS/$LIGHT_BATT1/delay_on
            echo $SLOW > $LEDS_CLASS/$LIGHT_BATT1/delay_off
            echo $OFF > $LEDS_CLASS/$LIGHT_BATT2/brightness
            echo $OFF  > $LEDS_CLASS/$LIGHT_BATT3/brightness
            echo $OFF  > $LEDS_CLASS/$LIGHT_BATT4/brightness
        fi
    fi
}

main()
{
    echo $OFF > $LEDS_CLASS/$LIGHT_WIFI/brightness

    echo $OFF > $LEDS_CLASS/$LIGHT_MODEM_BLUE/brightness
    echo $BLINK  > $LEDS_CLASS/$LIGHT_MODEM_RED/trigger
    echo $SLOW > $LEDS_CLASS/$LIGHT_MODEM_RED/delay_on
    echo $SLOW > $LEDS_CLASS/$LIGHT_MODEM_RED/delay_off

    batt_ctrl
}

close_other_led()
{
    echo $OFF > $LEDS_CLASS/$LIGHT_WIFI/brightness
    echo $OFF > $LEDS_CLASS/$LIGHT_SYS/brightness
    echo $OFF > $LEDS_CLASS/$LIGHT_MODEM_BLUE/brightness
    echo $OFF > $LEDS_CLASS/$LIGHT_MODEM_RED/brightness

    batt_ctrl
}

cmdline=$(cat /proc/cmdline)
bootreason="${cmdline##*boot_reason=}"
bootreason=${bootreason%% *}

if [ "${bootreason}" == "0" ]; then
    main
else
    close_other_led
fi